function Calendar(element, options){
  var self;
  var Element;
  var EventContainer;
  var Options = {
    days: 7,
    events: [],
    title: 'Kalender',
    icon: '',
    showTime: true,
    onEventClick: null
  }


  self = Object.defineProperties({},{
    'element': {
      get: function(){ return Element; },
      set: function(value) {
        if(typeof value == 'string'){
          Element = document.querySelector(value);
        }
        else if(value instanceof HTMLElement) {
          Element = value;
        }
      }
    },
    'events': {
      get: function(){
        return Options.events;
      },
      set: function(value){
        Options.events = value;
      }
    },
    'title': {
      get: function(){
        return Options.title;
      },
      set: function(value){
        Options.title = value;
      }
    },
    'icon': {
      get: function(){
        return Options.icon;
      },
      set: function(value){
        Options.icon = value;
      }
    },
    'showTime': {
      get: function(){
        return Options.showTime;
      },
      set: function(value){
        Options.showTime = value;
      }
    },
    'onEventClick': {
      get: function(){
        return Options.onEventClick;
      },
      set: function(value){
        Options.onEventClick = (typeof value === "function") ? value : null ;
      }
    }
  });

  self.element = element;

  Object.keys(options).forEach(function(key){
    if(key in Options) Options[key] = options[key];
  });

  function build(){
    Element.classList.add('widget','calendar');
    Element.innerHTML = '<div><h3>' + Options.title + '</h3>' + '<div style="background-image:url(' + Options.icon + ')"></div></div>';
    EventContainer = document.createElement('div');

    for(var i = 0;i < Options.days;i++){
      var day = document.createElement('div');
      var now = moment().startOf('day');
      var date = moment().startOf('day').add(i,'days');
      day.setAttribute('date',date.valueOf());
      day.setAttribute('label',date.calendar(now,{
        sameDay: '[i dag]',
        nextDay: '[i morgen]',
        nextWeek: 'dddd DD.MM',
        sameElse: 'dddd DD.MM'
      }))

      EventContainer.appendChild(day);
    }

    Element.appendChild(EventContainer);
  }

  function renderEvents(){
    Options.events.forEach(function(event){
      var startDate = moment(event.start);
      var time = moment(startDate).startOf('Day').valueOf();
      var container = EventContainer.querySelector('[date="' + time + '"]');
      if(!container) return;
      var entry = document.createElement('span');
      var timeString = (Options.showTime) ? startDate.format('HH.mm') + ' - ' : '';
      var link = document.createElement('a');
      link.href = event.link;
      link.innerHTML = event.title;
      entry.innerHTML = timeString;
      entry.appendChild(link);
      var ev = (Options.onEventClick) ? Options.onEventClick.bind(null,event) : null ;
      link.addEventListener('click', ev);

      container.appendChild(entry);
    });
  }

  build();
  renderEvents();

  return self;
};
