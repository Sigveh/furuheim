"use strict";

const gulp = require('gulp');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const concat = require('gulp-concat');

gulp.task('script',function(){
  gulp.src(['js/**/*.js'])
  .pipe(concat('app.js'))
  .pipe(gulp.dest('./build/js/'))
  .pipe(uglify())
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(gulp.dest('./build/js/'));
});

gulp.task('watch:script',['script'],function(){
  gulp.watch(['js/**/*.js'],['script']);
});
